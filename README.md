# Greg

Greg is a simple proxy that can be used to debug a production return portal locally. This also allows developing/testing the return portal without starting SCP and Orders, which can be useful.

# Installation

Greg is written in [Bun](https://bun.sh/), which needs to be installed to use it. To do so:

```shell
brew install bun
```

Don't use Brew? See the [installation instructions](https://bun.sh/docs/installation) for alternative methods.

# Using Greg

## Starting the proxy

To run Greg, execute the following in the directory:

```shell
bun proxy <brand-domain>
```

The brand domain argument specifies which brand to proxy requests to. For example, to send requests to https://letsfaceit.shipping-portal.com:

```shell
bun proxy letsfaceit
```

You should see the following output:

```shell
> $ bun index.ts letsfaceit
> Proxying localhost:3000 to brand letsfaceit
```

All requests sent to localhost:3000 will now go to the specified brand.

## Testing a portal

You'll need to update your `env.development` config in the return portal to point to Greg:

```env
VUE_APP_API_BASE_URL="http://localhost:3000/api/v2"
VUE_APP_SERVICE_POINTS_API_BASE_URL="https://servicepoints.sendcloud.sc/api/v2"
```

The servicepoints URL is also changed to completely match the experience of a user on production, but this is optional. You can leave it unchanged - this means that fetching servicepoints will fail, but the return portal will function fine. There's really no reason not to do this though.

Then just run your local portal as usual.

# Developing on Greg

Want to teach Greg some new skills? No problemo.
1. You'll want to install dependencies so that you have autocomplete: `bun install`
2. Run `bun dev <brand-domain>` 

The server will now automatically restart when changes are detected.
