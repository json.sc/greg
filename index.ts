const brand = Bun.argv[Bun.argv.length - 1]

const CORS_HEADERS = {
  "Access-Control-Allow-Origin": "*",
  "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, OPTIONS",
  "Access-Control-Allow-Headers": "*",
}

const API_URL = "panel.sendcloud.sc"

async function handleRequest(req: Request) {
  const url = new URL(req.url)
  url.href = url.href.replace("default", brand)
  url.host = API_URL
  url.protocol = "https"
  url.port = ""

  console.log(`${req.method} ${req.url} -> ${url.href}`)
  const body = req.body ? JSON.stringify(await req.json()) : undefined

  const headers: Record<string, string> = {}
  for (const [key, value] of req.headers.entries()) {
    if (["authorization", "content-type"].includes(key)) {
      headers[key] = value
    }
  }

  const result = await fetch(url, { body, headers, method: req.method})

  const json: unknown = await result.json()
  return Response.json(json, { status: result.status, headers: CORS_HEADERS })
}

const server = Bun.serve({ port: 3000, fetch: handleRequest });
console.log(`Proxying localhost:${server.port} to brand ${brand}`)
